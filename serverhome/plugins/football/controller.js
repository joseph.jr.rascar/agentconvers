const request = require('request');

function getNextMatchInformation(clubNameRaw, res){
    let clubSearch = clubNameRaw;

    let nextMatch;
    let nextTeam;
    var options = {
        url: '',
        headers: {
            'User-Agent': 'request',
            'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
        }
    };

    return new Promise((resolve, reject) => {

        resolve(getClubId(clubSearch));
        
    }).then((club)=>{
        options.url = "https://api.football-data.org/v2/teams/"+ club.id +"/matches?status=SCHEDULED"
        
        request(options, ((error, response, body)=>{
            nextMatch = JSON.parse(response.body);
            nextTeam = nextMatch.matches[0].awayTeam.name;
        }));

        return new Promise((resolve, reject) => {
            request(options, ((error, response, body)=>{
                nextMatch = JSON.parse(response.body);
                if(club.name == nextMatch.matches[0].awayTeam.name){
                    nextTeam = nextMatch.matches[0].homeTeam.name
                }else{
                    nextTeam = nextMatch.matches[0].awayTeam.name;
                }
                resolve(nextTeam);
            }));
            
        }).then((result)=>{
            res.end(JSON.stringify({resultText: result}));
        })
    })

}

function getClubId(teamName){
    let teamId;
    console.log("TEAMAMAMAMAMAMM");
    console.log(teamName);
    var options = {
        url: 'http://api.football-data.org/v2/teams?name='+teamName,
        headers: {
            'User-Agent': 'request',
            'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
        }
    };
    
    return new Promise((resolve, reject) => {
        request(options, ((error, response, body)=>{
            let team = JSON.parse(response.body);
            teamId = team.teams[0].id;

            
            team.teams.forEach((element)=>{
                let persoName2 = element.name.split(" ")[0] + " " + element.name.split(" ")[1];
                
                if(element.shortName.toUpperCase() == teamName.toUpperCase() || persoName2.toUpperCase() == teamName.toUpperCase()){
                    teamId = element.id;
                    resolve({'id':teamId, 'name': element.name});
                }
            })

            
        }));
        
    }).then((result)=>{
        return result;
    })
}

class FootballController {
        
        constructor(io){
            this.io = io;
        }
        
	postAction(req, res){
            switch(req.params.actionId){
                case "nextmatchof":
                    getNextMatchInformation(req.body.searchValue, res);
                    break;
                default:
                        res.end(JSON.stringify({}));
                    break;
            }
    }
    
    
}

module.exports = FootballController;