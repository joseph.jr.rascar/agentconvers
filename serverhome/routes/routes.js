//Setting routes
const fs = require('fs');
const request = require('request');



module.exports = function(app, io) {  
	
	app.get('/', function (req, res) {
		res.send('MerryHome server welcoms;p');
	});
    
    app.get('/getPlayerInformations', (req, res)=>{

        var options = {
            url: 'https://api.football-data.org/v2/teams/57/matches?status=SCHEDULED',
            headers: {
                'User-Agent': 'request',
                'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
            }
        };

        request(options, ((error, response, body)=>{
            console.log(body);
        }));
        res.send("Hello from the other side");

    })

    app.get('/getNextMatchInformation', (req, res)=>{


        
        let clubSearch = req.query.name;

        let nextMatch;
        let nextTeam;
        var options = {
            url: '',
            headers: {
                'User-Agent': 'request',
                'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
            }
        };

        return new Promise((resolve, reject) => {

            resolve(getClubId(clubSearch));
            
        }).then((club)=>{
            options.url = "https://api.football-data.org/v2/teams/"+ club.id +"/matches?status=SCHEDULED"
            
            request(options, ((error, response, body)=>{
                nextMatch = JSON.parse(response.body);
                nextTeam = nextMatch.matches[0].awayTeam.name;
            }));

            return new Promise((resolve, reject) => {
                request(options, ((error, response, body)=>{
                    nextMatch = JSON.parse(response.body);
                    if(club.name == nextMatch.matches[0].awayTeam.name){
                        nextTeam = nextMatch.matches[0].homeTeam.name
                    }else{
                        nextTeam = nextMatch.matches[0].awayTeam.name;
                    }
                    resolve(nextTeam);
                }));
                
            }).then((result)=>{
                res.send(result);
            })
        })

        

    })



    function getClubId(teamName){
        let teamId;
        console.log("TEAMAMAMAMAMAMM");
        console.log(teamName);
        var options = {
            url: 'http://api.football-data.org/v2/teams?name='+teamName,
            headers: {
                'User-Agent': 'request',
                'X-Auth-Token': '2cba6cbdd4e4442bb167afb7d777081e',
            }
        };
        
        return new Promise((resolve, reject) => {
            request(options, ((error, response, body)=>{
                let team = JSON.parse(response.body);
                teamId = team.teams[0].id;

                
                team.teams.forEach((element)=>{
                    let persoName2 = element.name.split(" ")[0] + " " + element.name.split(" ")[1];
                    
                    if(element.shortName.toUpperCase() == teamName.toUpperCase() || persoName2.toUpperCase() == teamName.toUpperCase()){
                        teamId = element.id;
                        resolve({'id':teamId, 'name': element.name});
                    }
                })

                
            }));
            
        }).then((result)=>{
            return result;
        })
    }
    

    //Search and load all plugins
	var pluginsFolder=__dirname+"/../plugins";
        var pluginsForMenu = [];
        var plugins= [];
	fs.readdir(pluginsFolder, (err, files) => {
            files.forEach(file => {
                //if controller exists add plugin controller url
                var path = pluginsFolder+"/"+file+"/controller.js";
                if (fs.existsSync(path)) {
                    plugins.push(file);
                    console.log("setting plugin "+file);
                    var classPluginController= require(path);
                    var tmpPluginController = new classPluginController(io);
                    app.post('/api/'+file+'/action/:actionId', tmpPluginController.postAction);
                    //if view exists add plugin view url
                    if(tmpPluginController.getView){
                        app.get('/api/'+file+'/view', tmpPluginController.getView);
                        pluginsForMenu.push(file);
                    }
                }
            });
	});
        
	//Url for getting a list of plugin view
	app.get('/api/plugins', function (req, res) {
		res.end(JSON.stringify(pluginsForMenu));
	});
        
        //Url for getting all plugins expressions
        app.get('/api/expressions', function (req, res) {
            var allExpression= {};
            for(var i=0; i<plugins.length; i++){
                var pathExpressions = pluginsFolder+"/"+plugins[i]+"/expressions.json";
                if (fs.existsSync(pathExpressions)) {
                    var expressions= require(pathExpressions);
                    allExpression[plugins[i]]= expressions;
                }
            }
            res.send(JSON.stringify(allExpression));
	});
	
        //Url for getting all plugins configs
        app.get('/api/configs', function (req, res) {
            var allConfigs= {};
            for(var i=0; i<plugins.length; i++){
                var pathConfig = pluginsFolder+"/"+plugins[i]+"/config.json";
                if (fs.existsSync(pathConfig)) {
                    var config= require(pathConfig);
                    allConfigs[plugins[i]]= config;
                }
            }
            res.send(JSON.stringify(allConfigs));
        });
	
        //config actions google assistant
	var {dialogflow} = require('actions-on-google');
	var assistant = dialogflow();
	var helloWorldIntent= require("../google_intents/HelloWordIntent");
	var androidTVIntent= require("../google_intents/AndroidTVIntent");
	assistant.intent('helloWorld', helloWorldIntent);
	assistant.intent('AndroidTV', androidTVIntent);
	app.post('/webhook', assistant);
}